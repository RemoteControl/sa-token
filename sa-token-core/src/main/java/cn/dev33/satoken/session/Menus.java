package cn.dev33.satoken.session;



import java.io.Serializable;
import java.util.List;

/**
 * 菜单格式（大部分系统只支持三级菜单，超过三级原则上不显示）
 */

public class Menus implements Serializable {
    private static final long serialVersionUID = 7072369370L;

    /**
     *  系统名称
     */
    private String sysName;

    /**
     * id
     */
    private String id;

    /**
     * 名称
     */
    private String name;

    /**
     * code 意义不大
     */
    private String code;
    /**
     * 图标（外部系统的图标最好不要显示，可能不兼容）
     */
    private String iconskip;
    /**
     * dir(目录),diy(自定义菜单),其他
     */
    private String type;
    /**
     * 是否打开 1=打开，0=关闭
     */
    private Integer open;

    /**
     * 跳转地址（本平台是相对路径，其他平台要求提供绝对路径）
     * 外部系统路径为：baseUrl+link
     */
    private String link;

    /**
     * 前置路径（给其他路径用的）,也是系统默认访问路径
     */
    private String baseUrl;

    /**
     * 子集（存在子集则当前type=dir）
     */
    private List<Menus> childList;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getIconskip() {
        return iconskip;
    }

    public void setIconskip(String iconskip) {
        this.iconskip = iconskip;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Integer getOpen() {
        return open;
    }

    public void setOpen(Integer open) {
        this.open = open;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public List<Menus> getChildList() {
        return childList;
    }

    public void setChildList(List<Menus> childList) {
        this.childList = childList;
    }

    public String getSysName() {
        return sysName;
    }

    public void setSysName(String sysName) {
        this.sysName = sysName;
    }
}
