package cn.dev33.satoken.session;



import java.io.Serializable;

/**
 * 应用信息
 */

public class AppInfo implements Serializable {
    private static final long serialVersionUID = 7072369371L;

    /**
     *  系统名称
     */
    private String sysName;

    /**
     * 系统域（如：http://www.xx.com/ 可以访问到此系统首地址）
     */
    private String sysDomain;


    public String getSysName() {
        return sysName;
    }

    public void setSysName(String sysName) {
        this.sysName = sysName;
    }

    public String getSysDomain() {
        return sysDomain;
    }

    public void setSysDomain(String sysDomain) {
        this.sysDomain = sysDomain;
    }
}
